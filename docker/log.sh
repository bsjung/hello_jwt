#!/bin/bash

# events
#kubectl get events --sort-by='.lastTimestamp' -n ingress-nginx
kubectl get events --sort-by='.lastTimestamp' -n test-web
#kubectl get events --sort-by='.lastTimestamp' -n prod-web
#kubectl get events --sort-by='.lastTimestamp' -n prod-ledger
#kubectl get events --sort-by='.lastTimestamp' -n prod-wallet
